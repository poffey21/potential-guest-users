# Potential Guest users

Identify users that could potentially be Guest users (and thus free users on an Ultimate subscription) based on their contributions.

This script will, for a configurable amount of days (the *checked period*), check user activity. It generates two lists:

* `inactive_users.csv`, containing users that have not logged in during the checked period
* `potential_guest_users.csv`, containing users that have not performed any tracked actions incompatible with the `Guest` permission.
  * The file contains some additional metadata to filter potential Guests more easily. The file will contain information on the highest role (10 to 50: Guest to Owner) the user has outside their personal namespace. It also contains how many projects and groups the user has Owner / Maintainer role in (outside their personal namespace), as well as how many personal projects the user has.
  * Because Maintainers and Owners often have further responsibilities, they are less likely targets for Guest users. However, being Owner of a personal project is not necessarily incompatible with being a Guest user.
  * These statistics can help you target, for example, Developer users that only perform Guest actions but have personal projects where they are Owners.

On self-managed GitLab, it will further generate:

* `no_membership_users.csv`, containing users that are not members of any groups or projects. These would definitely by considered "Guest" users, as they don't have any memberships higher than "Guest" level by definition. The report is based on [user's memberships](https://docs.gitlab.com/ee/api/users.html#user-memberships-admin-only). If a user is not member of any group or project, they will be added to this list. That also means they will already be counted as non-billable users on an Ultimate license, as their highest access level is even below `Guest`. They will not appear on `potential_guest_users.csv`.
* `guest_users.csv`, containing non-billable users that do have memberships, thus they are existing `Guest`users.

On SaaS, it will further genrate:

* `private_profile_users.csv`, containing users whose profile is private and thus can't be checked for guest status or last activity.

`potential_guest_users.csv` is based on the [user contribution events](https://docs.gitlab.com/ee/api/events.html#get-user-contribution-events), which tracks certain [actions](https://docs.gitlab.com/ee/user/index.html#user-contribution-events) of users. Many of these actions require higher than [Guest permissions](https://docs.gitlab.com/ee/user/permissions.html), for example pushing to repos or opening/approving MRs.

If during the checked period, no such events are found, the user is marked as potential Guest and added to `potential_guest_users.csv`.

**Note** that not all events are tracked, so `potential_guest_users.csv` may contain users executing actions that do require higher permissions.
For example, environment actions, viewing dashboards, cancelling jobs, managing variables, viewing merge requests etc are all not tracked as events and thus can not be checked by this report.

**Please view `potential_guest_users.csv` as rough estimate, based on those users not pushing, creating or approving merge requests and not destroying environments, i.e. not actively performing the core development tasks.**

## Configuration

### Self-managed

- Export / fork repository.
- Add a GitLab **admin** API token to CI/CD variables named `GITLAB_TOKEN`. Make sure it is "masked".
This token will be used to query the API for group and project members.
The token **must** be an admin token to be able to list all users and access their current sign in.
- Add your GitLab instance URL to CI/CD variables named `GITLAB_URL`.
- Configure `CHECK_PERIOD`: This is the period in days that users contributions are checked for. Defaults to 180 days
- Notice the Job is tagged `local` for testing purposes. Make sure your runners pick it up.

### GitLab.com (SaaS)

- Export / fork repository.
- Add a GitLab **group owner** API token (of the group you want to check) to CI/CD variables named `GIT_TOKEN`. Make sure it is "masked". This token will be used to query the API for group and project members. The token **must** be a group owner token to be able to get all group and project memberships of your group.
- Note that `GITLAB_TOKEN` is not used for the SaaS jobs, thus no admin token is needed here
- Configure `SAAS_GROUP`, the path or ID of the group you want to query
- Configure `CHECK_PERIOD`: This is the period in days that users contributions are checked for. Defaults to 180 days

## Usage

### Self-managed

`pip3 install -r requirements.txt`
`python3 identify-guest-users.py $GITLAB_URL $GITLAB_TOKEN [--check_period $CHECK_PERIOD]`

### GitLab.com (SaaS)

`pip3 install -r requirements.txt`
`python3 identify-guest-users-saas.py $GIT_TOKEN $MEMBERS_CSV [--check_period $CHECK_PERIOD]`

`$MEMBERS_CSV` refers to the output file of the [Group Member Report](https://gitlab.com/gitlab-com/cs-tools/gitlab-cs-tools/gitlab-group-member-report). So you have to run that report first, take its output CSV and put it into this script.

## Disclaimer

This script is provided for educational purposes. It is **not supported by GitLab**. However, you can create an issue if you need help or propose a Merge Request. This script only reads data via the GitLab API and does not perform write actions. For self-managed, this script requires an admin token, which is a very powerful credential. For SaaS, this script requires a group owner token, which is very powerful on your group as well. Make sure to rotate/expire the token regularly.

This script produces data file artifacts containing user names and (on self-managed) emails, i.e. personally identifiable information (PII). Please use this script and its outputs with caution and in accordance to your local data privacy regulations.

